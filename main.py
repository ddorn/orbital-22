from src.engine import App, BlackBordersScreen, FixedScreen, IntegerScaleScreen, SIZE
from src.states import GameState, MenuState

if __name__ == "__main__":
    # App(GameState, FixedScreen(SIZE)).run()
    App(MenuState, IntegerScaleScreen(SIZE)).run()
