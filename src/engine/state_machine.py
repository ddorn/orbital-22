from enum import Enum
from random import randint
from typing import Any, Callable, Generator, Iterator, List, Optional, ParamSpec, Type, TypeVar

import pygame.event
from pygame import Color, K_m, K_F11, K_q, K_ESCAPE

from .assets import play
from .constants import *
from .debug import Debug
from .gfx import GFX
from .object import Object, Scriptable
from .particles import ParticleSystem
from .pygame_input import Button, Inputs, JoyButton, QuitEvent
from .settings import settings

O = TypeVar("O", bound=Object)
P = ParamSpec("P")

__all__ = ["State", "StateMachine", "StateOperations"]


class StateOperations(Enum):
    NOP = 0
    POP = 1
    PUSH = 2
    REPLACE = 3


class State(Scriptable):
    """
    The State represent what's currently happening on the screen.

    It contains:
     - all objects and logic of a part of the game.
     - a particle system, which is drawn on top of all objects with a Z <= 0.
     - a debug system, drawn on top of everything

    The state is changed by calling pop/push/replace_state.
    """

    FPS: int = 60
    BG_COLOR: ColorType = "black"
    BG_MUSIC: str = 'forest.ogg'
    BG_COLORS: list[ColorType] = []
    BG_TRANSITION_TIME = 20 * 60

    def __init__(self) -> None:
        super().__init__()
        self.timer = 0
        self._to_add_later: set[Object] = set()
        self.objects: set[Object] = set()
        self.next_state: tuple[StateOperations, Optional[State]] = (StateOperations.NOP, self)
        self.shake = 0

        self.particles = ParticleSystem()

        self.debug = self.add(Debug())

        self.inputs = Inputs()

        self.add_script(self.script())

    def create_inputs(self) -> Inputs:
        pygame.joystick.init()
        nb = pygame.joystick.get_count()
        self.debug.text("Joysticks:", nb)
        if nb > 0:
            joy = pygame.joystick.Joystick(0)
            joy.init()
            self.joy = joy

        inputs = Inputs()
        inputs["quit"] = Button(QuitEvent(), K_ESCAPE, K_q, JoyButton(JOY_BACK)).on_press(self.pop_state)

        inputs["debug"] = Button(K_F11, JoyButton(10)).on_press(self.debug.toggle)

        inputs["mute"] = Button(K_m, JoyButton(11)).on_press(self.toggle_mute)

        for object in self.objects:
            obj_inputs = object.create_inputs()
            if not set(inputs).isdisjoint(obj_inputs):
                raise ValueError("Conflicting key inputs.")

            inputs.update(object.create_inputs())

        return inputs

    def toggle_mute(self, *_: Any) -> None:
        settings.mute = not settings.mute
        if settings.mute:
            pygame.mixer.music.set_volume(0)
        else:
            pygame.mixer.music.set_volume(1)

    # Life phase of state

    def on_resume(self) -> None:
        self.inputs = self.create_inputs()
        self.next_state = (StateOperations.NOP, None)
        self.debug.paused = False
        if self.BG_MUSIC and not settings.mute:
            pygame.mixer.music.load(MUSIC / self.BG_MUSIC)
            # pygame.mixer.music.set_volume(VOLUME['BG_MUSIC'] * Settings().music)
            pygame.mixer.music.play(-1)

    def on_exit(self) -> None:
        self.debug.paused = True

    def script(self) -> Iterator:
        """Script must be a generator where each yield will correspond to a frame.

        Useful to implement sequential logics.
        """
        yield

    def logic(self) -> None:
        """All the logic of the state happens here.

        To change to an other state, you need to call any of:
            - self.pop_state()
            - self.push_state(new)
            - self.replace_state(new)
       """
        super().logic()

        self.timer += 1

        self.update_bg()

        # Add all object that have been queued
        self.objects.update(self._to_add_later)
        for obj in self._to_add_later:
            self.inputs.update(obj.create_inputs())
        self._to_add_later = set()

        # Logic for all objects
        for object in self.objects:
            object.logic()
        self.particles.logic()

        # Clean dead objects
        to_remove = set()
        for object in self.objects:
            if not object.alive:
                to_remove.add(object)
                object.on_death()
        self.objects.difference_update(to_remove)

    def draw(self, gfx: "GFX") -> None:
        if self.BG_COLOR:
            gfx.fill(self.BG_COLOR)

        did_draw_particles = False
        for z in sorted(set(o.Z for o in self.objects)):
            for obj in self.objects:
                if z == obj.Z:
                    obj.draw(gfx)
            # We draw particles after the 0-th layer.
            if z >= 0 and not did_draw_particles:
                self.particles.draw(gfx.surf)
                did_draw_particles = True

        if not did_draw_particles:
            self.particles.draw(gfx.surf)

        if self.shake:
            s = 3
            gfx.scroll(randint(-s, s), randint(-s, s))
            self.shake -= 1

    def handle_events(self, events: list[pygame.event.Event]) -> None:
        self.inputs.trigger(events)
        for object in self.objects:
            object.handle_events(events)

    def resize(self, old: tuple[int, int], new: tuple[int, int]) -> None:
        for obj in self.objects:
            obj.resize(old, new)

    # State modifications

    def add(self, object: O) -> O:
        """Add an object to the state.

        Note that the object is only added at the beginning of the next frame.
        This allows to add objects while modifying the list.

        Returns:
            The argument is returned, to allow creating,
            adding and storing it in a variable in the same line.
        """

        self._to_add_later.add(object)
        object.state = self

        return object

    def get_all(self, *types: str | Type[Object]) -> Generator[Object, None, None]:
        """Get all objects in the State with the given types.

        Types can either be classes or class names."""
        for object in self.objects:
            for t in types:
                if isinstance(t, str):
                    if any(c.__name__ == t for c in object.__class__.__mro__):
                        yield object
                        break
                elif isinstance(object, t):
                    yield object
                    break

    def do_shake(self, frames: int) -> None:
        assert frames >= 0
        self.shake += frames

    def update_bg(self) -> None:
        if self.BG_COLORS:
            first = self.timer // self.BG_TRANSITION_TIME % len(self.BG_COLORS)
            second = (first + 1) % len(self.BG_COLORS)
            t = (self.timer % self.BG_TRANSITION_TIME) / self.BG_TRANSITION_TIME
            bg = Color(self.BG_COLORS[first]).lerp(self.BG_COLORS[second], t)
            # bg = mix(self.BG_COLORS[first], self.BG_COLORS[second], t)

            self.BG_COLOR = bg

    # State operations

    def pop_state(self, *_: Any) -> None:
        """Return to the previous state in the state stack."""
        self.next_state = (StateOperations.POP, None)

    def push_state(self, new: "State") -> None:
        """Add a state to the stack that will be switched to."""
        self.next_state = (StateOperations.PUSH, new)

    def replace_state(self, new: "State") -> None:
        """Replace the current state with an other one. Equivalent of a theoric pop then push."""
        self.next_state = (StateOperations.REPLACE, new)

    def push_state_callback(self, new: Type["State"], *args: Any) -> Callable:
        def callback(*_: Any) -> None:
            self.next_state = (StateOperations.PUSH, new(*args))

        return callback

    def replace_state_callback(self, new: Type["State"], *args: Any) -> Callable:
        def callback(*_: Any) -> None:
            self.next_state = (StateOperations.REPLACE, new(*args))

        return callback


class StateMachine:
    def __init__(self, initial_state: Type[State]):
        self._state: Union[State, None] = None
        self.stack: List[State] = []
        self.set_state(StateOperations.PUSH, initial_state())

    @property
    def running(self) -> bool:
        return len(self.stack) > 0

    @property
    def state(self) -> State:
        """Current state."""
        if self.stack:
            return self.stack[-1]
        raise ValueError("No current state.")

    def set_state(self, op: StateOperations, new: Optional[State] = None) -> None:
        if op == StateOperations.NOP:
            pass
        elif op == StateOperations.POP or new is None:  # Should be both
            if self.stack:
                prev = self.stack.pop()
                prev.on_exit()
                play("back")
            if self.stack:
                self.stack[-1].on_resume()
        elif op == StateOperations.REPLACE:
            if self.stack:
                prev = self.stack.pop()
                prev.on_exit()
            self.stack.append(new)
            new.on_resume()
        elif op == StateOperations.PUSH:
            if self.stack:
                self.stack[-1].on_exit()
            self.stack.append(new)
            new.on_resume()
        else:
            print(type(op).__module__, StateOperations.__module__)
            print(type(op) == StateOperations)
            print(op, new)
            raise ValueError("Unknown operation type.")
