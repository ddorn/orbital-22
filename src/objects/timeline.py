from src.engine import *
from src.objects.positioning import grid_to_rect

MIN_Y = 30
MAX_Y = H

TARGET_TIME = 120 # in seconds

class Timeline(Object):
    Z = -2
    def __init__(self):
        super().__init__((0, 0))
        self.fill_percent = 0

    def logic(self):
        super().logic()
        
        fill_speed = 100 / TARGET_TIME / self.state.FPS
        self.fill_percent = min(100, self.fill_percent + fill_speed)

    def draw(self, gfx: "GFX"):
        super().draw(gfx)
        ypos = H - MIN_Y - (MAX_Y - MIN_Y) * self.fill_percent / 100

        gfx.rect(1050, ypos, 220, H - ypos, "#00b97cff")
        gfx.blit(image("background", True))