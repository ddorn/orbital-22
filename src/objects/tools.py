from src.engine import *
from src.objects.positioning import GRID_CELL_WIDTH, grid_to_rect
from random import gauss, randrange


class Tool(SpriteObject):
    image_name = "spiderWebRed"  # there needs to be something, but it's not used.
    flip = False
    solid = True  # for the player

    OFFSET = (0, 0)
    SIZE = (GRID_CELL_WIDTH, GRID_CELL_WIDTH)

    def __init__(self, tile: tuple[int, int], rot=0):
        self.tile = tile
        img = image(self.image_name)
        img = pygame.transform.flip(img, self.flip, False)
        super().__init__(grid_to_rect(tile), img, self.OFFSET, self.SIZE, rotation=rot)


class Bouncer(Tool):
    OFFSET = (-8, -5)

    def __init__(self, tile, color: int, flipped: bool):
        self.color = color
        self.flip = flipped
        self.image_name = f"spiderWeb{COLORS[color]}"
        super().__init__(tile)

    @classmethod
    def mk(cls, color: int, flipped: bool = False):
        return lambda tile: cls(tile, color, flipped)

class Ventilation(Tool):
    OFFSET = (5, 12)
    WIND_POWER = 10

    TIME_OFF = 130
    TIME_ON = 130

    ROT_MAP = {'horiz': 90, 'vert': 180}
    # guche -> drte; bas -> ht

    def __init__(self, tile, rot: str):
        self.image_name = "ventilo"
        self.rot = rot
        self.on = False
        self.last_action = 0
        super().__init__(tile, self.ROT_MAP[rot])

    @classmethod
    def mk(cls, rot):
        return lambda tile: cls(tile, rot)

    def logic(self):
        super().logic()

        if self.on and self.state.timer > self.last_action + self.TIME_ON:
            self.on = False
            self.last_action = self.state.timer
        if (not self.on) and self.state.timer > self.last_action + self.TIME_OFF:
            self.on = True
            self.last_action = self.state.timer

    def draw(self, gfx: "GFX"):
        super().draw(gfx)

        if self.on:
            if self.rot == 'horiz':
                for x in range(8):
                    self.state.particles.add(
                        SquareParticle("#ffffff")
                            .builder()
                            .at(self.center + (x * 50 + 25, randrange(50) - 25), 0)
                            .velocity(gauss(6, 2))
                            .sized(2)
                            .inner_rotation(uniform(0, 360), 2)
                            .living(15)  # Frames
                            .anim_fade()
                            .build()
                    )
            else:
                for y in range(8):
                    self.state.particles.add(
                        SquareParticle("#ffffff")
                            .builder()
                            .at(self.center + (randrange(50) - 25, - y * 50 - 25), -90)
                            .velocity(gauss(6, 2))
                            .sized(2)
                            .inner_rotation(uniform(0, 360), 2)
                            .living(15)  # Frames
                            .anim_fade()
                            .build()
                    )


class Wall(Tool):

    def draw(self, gfx: "GFX"):
        pass

SPAWN_TIME = 100
class Spawner(Tool):
    image_name = "plantule1"
    def __init__(self, tile):
        super().__init__(tile)
        self.spawned = True
        self.next_spawn = SPAWN_TIME

    def logic(self):
        super().logic()

        if not self.spawned:
            if self.next_spawn == 0:
                self.spawned = True
                self.next_spawn = SPAWN_TIME
            self.next_spawn -= 1

    def draw(self, gfx: GFX):
        super().draw(gfx)
        if self.spawned:
            gfx.blit(image("orbWhite"), center=self.sprite_center+(5, -25))