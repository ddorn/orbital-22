from random import choice

import pygame.mouse
from pygame.locals import *
from pygame import Vector2

from src.engine import *
from src.objects.game_colors import color_code, color_name
from src.objects.positioning import *

ARRIVE_FROM_LEFT = 1
ARRIVE_FROM_RIGHT = -1

# liste image, offset pour chaque pnj pour aligner sa tête dans sa case
PNJs = [
    ("pnj1", (-50, 0)),
    ("pnj2", (10, 0)),
    ("pnj3", (-20, 0)),
    ("pnj4", (-40, -20)),
    ("pnj5", (-20, -35)),
    ("pnj6", (-20, -25)),
]

NPC_OFFSET = (-70, -15)

class Npc(SpriteObject):
    def __init__(self, grid_pos, mood: list[bool], sentence: str, draw_border: bool):
        self.draw_border = draw_border
        self.grid_pos = grid_pos
        self.target_pos = Vector2(grid_to_rect(grid_pos))

        self.arriving = True
        self.arrive_from = ARRIVE_FROM_LEFT if grid_pos[0] < GRID_WIDTH // 2 else ARRIVE_FROM_RIGHT

        initial_x = -GRID_CELL_WIDTH if self.arrive_from == ARRIVE_FROM_LEFT else W

        self.sentence = sentence
        self.text = None

        self.mood = mood
        self.has_orb = False
        self.orb_colors = None


        # Sprite image with an offset to compensate the fact that the image is not the size of the grid cell
        self.pnj = choice(PNJs)
        npc_image = image(self.pnj[0])
        npc_image = pygame.transform.flip(npc_image, self.arrive_from == ARRIVE_FROM_RIGHT, False)
        bouche_x, bouche_y = self.pnj[1]
        img_size = npc_image.get_size()
        super().__init__(
            Vector2(initial_x, self.target_pos[1]),
            npc_image,
            Vector2((GRID_CELL_WIDTH-img_size[0])/2+self.arrive_from*bouche_x, -(img_size[1]-GRID_CELL_WIDTH)/2+bouche_y) + self.npc_offset
        )
    def receive(self, projectile):
        self.has_orb = True
        self.orb_colors = projectile.colors
        self.check(projectile)

    def check(self, projectile):
        if all(self.mood[i] == projectile.colors[i] for i in range(3)):
            play('happy')
            self.add_script(self.leave())
        else:
            play('shield') # play('angry')
            self.add_script(self.drop_orb())
            
    def drop_orb(self):
        self.define_text("I don't feel like it...")
        yield from range(60 * 3)
        self.define_text(self.sentence)
        self.has_orb = False

    def leave(self):
        self.define_text("Thank you berry much!")
        yield from range(60 * 4)

        self.define_text(None)

        self.arriving = True
        while self.rect.colliderect(SCREEN):
            current_vel = int(2 + abs(self.target_pos[0] - self.pos[0])/W*15)
            self.pos[0] -= current_vel*self.arrive_from
            yield

        if self in self.state.map.npc_scheduled:
            self.state.map.npc_scheduled.remove(self)

        self.alive = False

    def script(self):
        while abs(self.target_pos[0] - self.pos[0]) > 4: # Si on a atteint la target
            current_vel = int(2 + abs(self.target_pos[0] - self.pos[0])/W*15)
            self.pos.x += current_vel*self.arrive_from
            yield

        self.pos = self.target_pos
        self.arriving = False
        self.define_text(self.sentence)

    def logic(self):
        super().logic()

    def draw(self, gfx: "GFX"):
        super().draw(gfx)
        if self.text != None:
            # Boite derrière le texte
            inflate = 10
            if self.draw_border:
                gfx.rect(*self.text.get_rect(center=self.text_center).inflate(10, 10), color_code(self.mood), border_radius=5)
                inflate -= 6
            gfx.rect(*self.text.get_rect(center=self.text_center).inflate(inflate, inflate), "white", border_radius=5)
            # Le texte en lui même
            gfx.blit(self.text, center=self.text_center)

            # petit triangle pour faire classe
            offset = Vector2(self.text.get_width() // 2 + 3, 0)
            triangle = [offset + (0,-10), offset + (0, 10), offset+(15,0)]
            pygame.draw.polygon(surface=gfx.surf, color="white", points=[self.text_center+self.arrive_from*pt for pt in triangle])

        if not self.arriving: # draw the bowl
            gfx.blit(image("bowlBack"), center=self.bowl_center)
            if self.has_orb:
                orb_image = image(f"orb{color_name(self.orb_colors)}")
                gfx.blit(orb_image, center=self.bowl_center+(0, -10))
            gfx.blit(image("bowlFront"), center=self.bowl_center)

    
    @property
    def npc_offset(self):
        return (
            self.arrive_from*NPC_OFFSET[0],
            NPC_OFFSET[1],
        )

    @property
    def text_center(self):
        """Position of the center of the sprite, in world coordinates."""
        return (
            self.pos + self.npc_offset
            + (GRID_CELL_WIDTH if self.arrive_from == ARRIVE_FROM_RIGHT else 0, 0)
            + (-self.arrive_from*(self.text.get_width()//2 + 25), GRID_CELL_WIDTH//2)
        )

    @property
    def bowl_center(self):
        return (
            self.pos + self.npc_offset + (GRID_CELL_WIDTH/2, GRID_CELL_WIDTH/2) +
            (self.arrive_from * 70, 20)
        )

    def define_text(self, t):
        if t:
            self.text = wrapped_text(t, 15, "black", 200, "Wellbutrin")
        else:
            self.text = None
