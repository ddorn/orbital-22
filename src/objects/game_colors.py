colors = {
    "000": "White",
    "001": "Red",
    "010": "Blue",
    "011": "Purple",
    "100": "Yellow",
    "101": "Orange",
    "110": "Green",
    "111": "Black",
}


def color_name(inst_colors):
    return colors["".join(["1" if inst_colors[i] else "0" for i in range(3)][::-1])]


def color_inst(color):
    key_list = list(colors.keys())
    val_list = list(colors.values())

    position = val_list.index(color)
    inst = key_list[position]

    return [True if c == "1" else False for c in inst][::-1]


def color_code(inst_colors):
    hex_colors = [
        "#ffffff",  # 000
        "#ef0000",  # 001
        "#2b71d1",  # 010
        "#db00cf",  # 011
        "#f5d900",  # 100
        "#ff6701",  # 101
        "#27ff53",  # 110
        "#424242",  # 111
    ]
    return hex_colors[sum(inst_colors[i] * 2 ** i for i in range(3))]
