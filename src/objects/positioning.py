from src.engine import *

GRID_WIDTH = 16
GRID_HEIGHT = 14

# we leave some margin on the sides
# available_width = int(0.8*W)
# available_height = int(0.8*H)

# We compute the grid size
# GRID_CELL_WIDTH = min(available_height//GRID_HEIGHT, available_width//GRID_WIDTH)
GRID_CELL_WIDTH = 50

grid_left_start = (W - GRID_CELL_WIDTH*GRID_WIDTH)//2
grid_top_start = (H - GRID_CELL_WIDTH*GRID_HEIGHT)//2

def grid_to_rect(pos):
    return (grid_left_start+pos[0]*GRID_CELL_WIDTH, grid_top_start+pos[1]*GRID_CELL_WIDTH)

def rect_to_grid(world_pos):
    return (int((world_pos[0] - grid_left_start) / GRID_CELL_WIDTH),
            int((world_pos[1] - grid_top_start) / GRID_CELL_WIDTH))


# WALLS
LEFT_WALL = [
    (3, 0),
    (4, 0),
    (5, 0),
    (6, 0),
    (7, 0),
    (8, 0),
    (9, 0),
    (10, 0),
    (11, 0),
    (3, 1),
    (3, 2),
    (2, 2),
    (1, 2),
    (1, 3),
    (1, 4),
    (1, 5),
    (1, 6),
    (1, 7),
    (1, 8),
    (1, 9),
    (1, 10),
    (1, 11),
    (2, 11),
    (3, 11),
    (3, 12),
    (4, 12),
]
RIGHT_WALL = [
    (11, 1),
    (12, 1),
    (13, 1),
    (13, 2),
    (13, 3),
    (14, 3),
    (14, 4),
    (14, 5),
    (14, 6),
    (14, 7),
    (14, 8),
    (14, 9),
    (13, 9),
    (13, 10),
    (13, 11),
    (12, 11),
    (12, 12),
    (11, 12),
    (11, 13),
    (10, 13),
    (9, 13),
    (8, 13),
    (7, 13),
    (6, 13),
    (5, 13),
    (4, 13),
]