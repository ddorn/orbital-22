import random
from itertools import chain
from random import gauss, randint

import pygame.mouse
import pygame.gfxdraw
from pygame.locals import *
from pygame import Vector2

from src.engine import *
from src.objects.game_colors import color_code, color_name
from src.objects.positioning import GRID_CELL_WIDTH, grid_to_rect, LEFT_WALL, rect_to_grid, RIGHT_WALL
from src.objects.tools import Bouncer, Spawner, Tool, Wall

class Projectile(SpriteObject):
    OK_RADIUS = 50
    VEL_STEP = 0.01
    VENT_POWER = 3
    VENT_DIST = 8
    MAX_VEL = 1.8

    def __init__(self, pos: Vector2, vel: Vector2, fake=False):
        super().__init__(pos, smoothscale(image("orb1"), 0.9), vel=vel * self.MAX_VEL)
        self.last_bounce = None
        self.radius = 5
        self.fake = fake
        self.colors = [False, False, False]

    @property
    def color(self):
        return color_code(self.colors)

    def bounce(self, flipped):
        if not flipped:
            self.vel[0], self.vel[1] = self.vel[1], self.vel[0]
        else:
            self.vel[0], self.vel[1] = -self.vel[1], -self.vel[0]

        self.vel *= 1.2

    def handle_bounce(self, bouncer: Bouncer):
        if not self.fake:
            play('bounce')

        if bouncer is self.last_bounce:
            return

        r = bouncer.rect
        if bouncer.flip:
            off = Vector2(1, -1) * 10
            points = [r.topright, r.bottomleft]
        else:
            off = Vector2(1, 1) * 10
            points = [r.topleft, r.bottomright]

        p0 = Vector2(points[0]) + off
        p1 = Vector2(points[1]) - off

        distance = part_perp_to(p0 - self.center, p1 - p0)

        self.state.debug.vector(distance, self.center)

        if distance.length() < self.radius and distance.dot(self.vel) > 0 and self.rect.colliderect(bouncer.rect):
            self.last_bounce = bouncer
            self.bounce(bouncer.flip)

            if bouncer.color != NEUTRAL_c:
                if not self.fake:
                    self.colors[bouncer.color] = True
                    for i in range(int(gauss(200, 30))):
                        self.state.particles.add(
                            CircleParticle(self.color)
                                .builder()
                                .at(bouncer.center, gauss(180 + self.vel.as_polar()[1], 30))
                                .velocity(gauss(3, 1))
                                .sized(3)
                                .living(20)  # Frames
                                .anim_fade()
                                .build()
                        )

    def check_collisions(self):
        npcs = self.state.get_all("Npc")
        ventilos = self.state.map.ventilos

        if not self.fake:
            for npc in npcs:
                if (npc.bowl_center - self.pos).length() < self.OK_RADIUS:
                    npc.receive(self)
                    self.alive = False

        for ventilo in ventilos:
            if ventilo.on and ventilo.rot == 'horiz' and ventilo.tile[0] < rect_to_grid(self.pos)[0] and ventilo.tile[1] == rect_to_grid(self.pos)[1] and ventilo.tile[0] + self.VENT_DIST > rect_to_grid(self.pos)[0]:
                self.pos += (self.VENT_POWER, 0)
            elif ventilo.on and ventilo.rot == 'vert' and ventilo.tile[0] == rect_to_grid(self.pos)[0] and ventilo.tile[1] > rect_to_grid(self.pos)[1] and ventilo.tile[1]  - self.VENT_DIST < rect_to_grid(self.pos)[1]:
                self.pos += (0, -self.VENT_POWER)

        for tool in self.state.map.around(rect_to_grid(self.center)):
            assert isinstance(tool, Tool)
            if not self.fake:
                for npc in npcs:
                    if (npc.bowl_center - self.pos).length() < self.OK_RADIUS:
                        npc.receive(self)
                        self.alive = False

            if not tool.rect.inflate(self.radius, self.radius).collidepoint(*self.center):
                pass
            elif isinstance(tool, Wall):
                pass
            elif isinstance(tool, Spawner):
                self.alive = False
            elif isinstance(tool, Bouncer):
                self.handle_bounce(tool)
            else:
                pass

    def on_death(self):
        for i in range(400):
            self.state.particles.add(CircleParticle(self.color).builder()
                                     .at(self.pos, uniform(0, 360))
                                     .velocity(1.1 ** uniform(0, 10)).sized(gauss(5, 2)).living(50).acceleration(-0.3)
                                     .anim_fade(0.7).build()
    )

    def logic(self):
        self.base_image = image(f"orb{color_name(self.colors)}")

        speed = self.vel.length()
        if speed > 0.1:
            self.vel /= speed
            self.vel *= max(0, speed - self.VEL_STEP)
        else:
            play('explosion')
            self.alive = False

        # UGLY AF
        vel, self.vel = self.vel, Vector2()
        super().logic()
        iters = 5
        for i in range(iters):
            self.pos += vel / iters
            self.vel = vel
            self.check_collisions()
            self.vel = Vector2()
        self.vel = vel

        if not self.fake:
            self.state.particles.add(SquareParticle(self.color)
                                     .builder()
                                     .at(self.center + from_polar(gauss(0, 5), uniform(0, 360)), gauss(180 + self.vel.as_polar()[1], 20))
                                     .sized(3)
                                     .living(30)
                                     .velocity(1).anim_fade()
                                     .build()
                                     )

        if not self.rect.colliderect(SCREEN):
            self.alive = False


    def draw(self, gfx: "GFX"):
        super().draw(gfx)

class Player(SpriteObject):
    Z = 1
    SPEED = 5
    MAX_FRONDE_LENGTH = 120
    FROND_WIDTH = 10
    ANTENNAS = [
        [(3, 3), (63, 3)]  # Left
        # Up
        # Right
        # Down
    ]
    BZ_SMOOTHNESS = 1_000
    OFFSET = (-22, -43)
    SIZE = (30, 56)
    MUSIC_DELAY = 10

    def __init__(self, pos: Vector2):
        img = image("perso")
        w, h = img.get_size()
        super().__init__(pos, img, self.OFFSET, self.SIZE)

        self.click_for_shoot = False
        self.shooting = False
        self.shoot_offset = Vector2()
        self.mouse = Vector2()
        self.tools = [3, 0, 1]
        self.selected_tool = 0
        self.build_tile = None
        self.heading = 0  # angle

        self.loaded = False
        self.last_music_time = 0

    def create_inputs(self):
        vertical = Axis(
            [K_UP, K_w, K_t],
            [K_DOWN, K_s, K_g],
            JoyAxis(JOY_VERT_LEFT),
            JoyAxis(JOY_VERT_RIGHT)
        )
        horizontal = Axis(
            [K_LEFT, K_a, K_f],
            [K_RIGHT, K_d, K_d, K_h],
            JoyAxis(JOY_HORIZ_LEFT),
            JoyAxis(JOY_HORIZ_RIGHT)
        )
        vertical.always_call(self.set_vel_y)
        horizontal.always_call(self.set_vel_x)

        click = Button(MouseButtonPress(1))

        @click.on_press
        def _(_):
            self.click_for_shoot = True
            self.shoot_offset = Vector2(pygame.mouse.get_pos())

        @click.on_release
        def _(_):
            self.click_for_shoot = False
            if self.shooting and self.loaded:
                self.shoot()

        # create = Button(K_SPACE)
        # create.on_press(self.build)

        controls = [vertical, horizontal, click]
        return dict(zip(controls, controls))

    def handle_events(self, events):
        for event in events:
            if event.type == MOUSEBUTTONDOWN:
                if event.button == 4:
                    self.next_tool()
                elif event.button == 5:
                    self.next_tool(-1)

    def next_tool(self, change=1):
        if any(self.tools):
            self.selected_tool += change
            self.selected_tool %= len(self.tools)
            while self.tools[self.selected_tool] == 0:
                self.selected_tool += change
                self.selected_tool %= len(self.tools)

    # def build(self, _):
    #     if self.tools[self.selected_tool] > 0 and self.build_tile:
    #         self.state.add(TOOLS[self.selected_tool](self.build_tile))
    #         self.tools[self.selected_tool] -= 1
    #     else:
    #         play("denied")

    def steps_sound(self):
        if self.state.timer > self.last_music_time + self.MUSIC_DELAY:
            self.last_music_time = self.state.timer
            play('insect_walking')

    def set_vel_x(self, axis: Axis):
        if axis.value != 0:
            self.steps_sound()
        self.vel.x = axis.value * self.SPEED

    def set_vel_y(self, axis: Axis):
        if axis.value != 0:
            self.steps_sound()
        self.vel.y = axis.value * self.SPEED

    def shoot(self):
        assert self.shooting
        self.shooting = False
        self.loaded = False

        pos, vel = self.shoot_data()
        play('shoot')
        self.state.add(Projectile(pos, vel))

    def shoot_data(self) -> tuple[Vector2, Vector2]:
        offset = (self.mouse - self.shoot_offset) / 2
        clamp_length(offset, self.MAX_FRONDE_LENGTH)
        P0 = Vector2(self.ANTENNAS[0][0])
        P1 = sum(self.ANTENNAS[0], start=Vector2()) / 2 + offset
        P2 = Vector2(self.ANTENNAS[0][1])
        off = (-15 * 0.9, -31 * 0.9) if offset.y >= 0 else (-15 * 0.9, 0)
        pos = P0 * .25 + P1 * .5 + P2 * .25 + self.sprite_to_screen((0, 0)) + off
        vel = ((Vector2(self.ANTENNAS[0][0]) + Vector2(self.ANTENNAS[0][1])) / 2 - P1) / 30

        return pos, vel

    def logic(self):
        self.mouse = Vector2(pygame.mouse.get_pos())

        if self.vel.length() > self.SPEED / 2:
            self.heading = self.vel.as_polar()[1]

        # find the build tile
        self.build_tile = None
        for i in range(1, 10):
            tile = rect_to_grid(self.center + from_polar(GRID_CELL_WIDTH * i / 3, self.heading))
            if tile in self.state.map.cells:
                break
            if not self.rect.colliderect(*grid_to_rect(tile), GRID_CELL_WIDTH, GRID_CELL_WIDTH):
                self.build_tile = tile
                break


        # collisions
        tile = rect_to_grid(self.center)
        for dx in range(-2, 3):
            for dy in range(-2, 3):
                pos = tile[0] + dx, tile[1] + dy
                if tool := self.state.map.cells.get(pos):
                    rect = tool.rect
                    if rect.colliderect(self.rect):
                        combined = self.rect.inflate(rect.size)
                        direction = (Vector2(rect.center) - combined.center)

                        if isinstance(tool, Spawner):
                            # Si c'est un spawner, on veut lui prendre sa graine
                            if tool.spawned:
                                self.loaded = True
                                tool.spawned = False
                            continue

                        # Sinon
                        normalized_direction = direction.normalize()
                        out_line = combined.clipline(rect.center, rect.center + normalized_direction * 100)
                        force = normalized_direction * (Vector2(out_line[0]) - out_line[1]).length() / 2
                        self.vel -= force


        clamp_length(self.vel, 5)
        super().logic()

        self.state.debug.text(self.click_for_shoot,
                              self.shoot_offset, self.shooting)

        # Shoot mechanism
        if self.click_for_shoot and self.loaded and self.shoot_offset.distance_to(self.mouse) > 15:
            self.shooting = True

        if self.vel.length() > self.SPEED / 2 and random.random() < 0.1:
            self.state.particles.add(
                CircleParticle("#300000")
                .builder()
                .at(self.rect.midbottom + Vector2(gauss(0, 10), 0), gauss(DOWNWARDS, 50))
                .velocity(0.2)
                .sized(randint(3, 9))
                .living(40)
                .anim_fade(-1.)
                .anim_shrink()
                .build()
            )

    def draw(self, gfx: "GFX"):
        super().draw(gfx)

        if self.loaded and not self.shooting:
            gfx.blit(scale(image("orbWhite"), 0.8), center=self.sprite_center+(20, 25))

        if self.shooting:
            offset = (self.mouse - self.shoot_offset) / 2
            clamp_length(offset, self.MAX_FRONDE_LENGTH)

            P0 = Vector2(self.ANTENNAS[0][0])
            P1 = sum(self.ANTENNAS[0], start=Vector2()) / 2 + offset
            P2 = Vector2(self.ANTENNAS[0][1])
            pos = self.sprite_to_screen((0, 0))
            self.state.debug.point(*offset)

            bz_curve = [P0 * ((1-t) ** 2) + P1 * (2 * t * (1-t)) + P2 * (t ** 2) +
                        pos for T in range(self.BZ_SMOOTHNESS) if (t := T / self.BZ_SMOOTHNESS)]

            pygame.draw.lines(gfx.surf, "black", False, bz_curve, 4)

            nut = smoothscale(image("orbWhite"), 0.9)

            if offset.y >= 0:
                gfx.blit(nut, midbottom=P0 * .25 + P1 * .5 + P2 * .25 + pos)
            else:
                gfx.blit(nut, midtop=P0 * .25 + P1 * .5 + P2 * .25 + pos)

            trajectory = self.compute_trajectory()
            if trajectory:
                last = trajectory[0]
                second_last = trajectory[0]
                filtered = [last]
                for p in trajectory[1:]:
                    if p.distance_to(filtered[-1]) > 20 or (p - last).dot(last - second_last) < 0:
                        pygame.gfxdraw.aacircle(gfx.surf, *vec2int(p), 3, WHITE)
                        filtered.append(p)
                    last, second_last = p, last
                # pygame.draw.lines(gfx.surf, 0x9988aa, False, filtered)

        # Showing tools
        if False:  # any(self.tools)
            y = 10
            x = 20
            padding = 5
            for tool, nb in enumerate(self.tools):
                name = TOOLS[tool].name
                img = image(name)
                if nb == 0:
                    img = overlay(img, "black", 100)
                if tool == self.selected_tool:
                    img = outline(img, YELLOW)
                    img.set_colorkey(0)
                    img = outline(img, YELLOW)
                    img.set_colorkey(0)
                r = gfx.blit(img, topleft=(x, y))
                x += img.get_width() + padding
                gfx.text(str(nb), 20, YELLOW,
                         bottomright=r.bottomright - Vector2(2, 2))

        # Show the build tile if needed
        if self.tools[self.selected_tool] and self.build_tile:
            tl = grid_to_rect(self.build_tile)
            gfx.rect(*tl, GRID_CELL_WIDTH, GRID_CELL_WIDTH, YELLOW, 1, border_radius=4)

    def compute_trajectory(self):
        proj = Projectile(*self.shoot_data(), True)

        proj.state = self.state

        points = []
        while proj.vel.length() > 0.1 and proj.alive:
            proj.logic()
            points.append(proj.center.copy())

        return points
