from typing import Iterable, Iterator

import pygame.mouse
from pygame.locals import *
from pygame import Vector2, Rect, Color

from src.engine import *
from src.objects.game_colors import color_inst
from src.objects.positioning import *

from src.objects.npc import Npc

# To copy
from src.objects.tools import *

class LevelNPC:
    """
    moods: array of
        "White"
        "Red"
        "Blue"
        "Purple"
        "Yellow"
        "Orange"
        "Green"
        "Black"
    """
    def __init__(self, sentence: str, grid_pos, mood):
        self.sentence = sentence
        self.grid_pos = grid_pos
        self.mood = mood

    def instanciate(self, draw_border: bool):
        return Npc(self.grid_pos, color_inst(self.mood[0]), self.sentence, draw_border)

TIP_BOTTOM = 0
TIP_TOP = 1
class Level:
    def __init__(self, map: str, PNJs: list, draw_border: bool = True, tip = None, tip_pos = TIP_BOTTOM):
        self.map = map
        self.PNJs = PNJs
        self.draw_border = draw_border
        self.tip = tip
        self.tip_pos = tip_pos

base = Level("""\
....XXXXXXXX....
...XX......XXX..
.XXX.........X..
.X...........XX.
.X............X.
.X............X.
.X............X.
.X............X.
.X............X.
.X...........XX.
.X...........X..
.XXX........XX..
...XX......XX...
....XXXXXXXX....""", [])

level1 = Level("""\
....XXXXXXXX....
...XX......XXX..
.XXX.........X..
.X...........XX.
.X............X.
.X.....Y......X.
.X............X.
.X.S......r...X.
.X............X.
.X...........XX.
.X...y.......X..
.XXX........XX..
...XX......XX...
....XXXXXXXX....""", [
    LevelNPC("I'm excited to try out this bar, I heard they serve the most amazing drinks according to our feelings. Barman, show me what you got!", (2, 1), ["Yellow"]),
    LevelNPC("Ay mate! I had the most amazing day, let’s celebrate!", (1, 4), ["Yellow"]),
    LevelNPC("I need a drink lah, the vibration created by the newly arrived Creatures are driving me insane!", (8, 13), ["Red"]),
    LevelNPC("I like those Creatures, they release hormones that taste stunningly good!", (14, 8), ["Yellow"]),
    LevelNPC("Hurry up, something strong to mask the awful smell that these Creatures give off.", (14, 2), ["Red"]),
], tip="tip2")

level2 = Level("""\
....XXXXXXXX....
...XX......XXX..
.XXX...S.....X..
.X...........XX.
.X...Y........X.
.X............X.
.X.V........y.X.
.X............X.
.X............X.
.X...Y..r....XX.
.X...........X..
.XXX........XX..
...XX......XX...
....XXXXXXXX....""", [
    LevelNPC("I’m having trouble sleeping what with the noisy Creatures, but I’m confident they’ll become quieter once they settle down.", (13, 9), ["Orange"]),
    LevelNPC("Isn’t it pleasant that the Creatures break the boredom of the forest ?", (2, 9), ["Yellow"]),
    LevelNPC("The Creatures just went on and destroyed an entire neighborhood, unbelievable! Fortunately I don’t live there.", (4, 12), ["Orange"]),
    LevelNPC("You better give me a drink or I’ll break something, I swear!", (12, 1), ["Red"]),
    LevelNPC("At least I’m not the only one that wants to beat up those stinky Creatures.", (12, 1), ["Orange"]),
], False, tip="tip", tip_pos=TIP_TOP)

level3 = Level("""\
....XXXXXXXX....
...XX......XXX..
.XXX....b....X..
.X...........XX.
.X..B.....y...X.
.X............X.
.X............X.
.X............X.
.X........R...X.
.X...........XX.
.X.....b.....X..
.XXX........XX..
...XX...S..XX...
....XXXXXXXX....""", [
    LevelNPC("My sister died this morning, the funeral is in a few hours.", (2, 4), ["Blue"]),
    LevelNPC("Lately I want to kick the bucket, but then I remember that you still owe me a drink.", (12, 10), ["Blue"]),
    LevelNPC("Those stinky Creatures poisoned my brother, I want nothing more than to punch them in the face!", (12, 2), ["Purple"]),
    LevelNPC("My companion fled the forest. Good for him, although I do miss him now.", (2, 8), ["Green"]),
    LevelNPC("What nerve the Creatures have to take everything away from me and to leave me alone without any home or family.", (12, 12), ["Purple"]),
    LevelNPC("The perspective of the endless nothingness is oddly delightful: all the loneliness and hostility the Creatures created will end soon.", (4, 12), ["Black"]),
], False)

NB_PNJs = 5
# TIME_BETWEEN_NPCs = 0
TIME_BETWEEN_NPCs = 10 # en s

LEVELS = [level1, level2, level3]

class Map(Object):
    Z = -1

    def __init__(self, level: Level):

        self.last_pnj_entry = -float("inf")
        self._state = None
        self.cells: dict[tuple[int, int], Tool | None] = {}
        self.ventilos = []

        self.level = level
        # self.level = level3

        assert len(level.map.splitlines()) == 14
        for y, row in enumerate(level.map.splitlines()):
            assert len(row) == 16
            for x, char in enumerate(row):
                constructor = {
                    "X": Wall,
                    "r": Bouncer.mk(RED_c),
                    "b": Bouncer.mk(BLUE_c),
                    "y": Bouncer.mk(YELLOW_c),
                    "n": Bouncer.mk(NEUTRAL_c),
                    "R": Bouncer.mk(RED_c, True),
                    "B": Bouncer.mk(BLUE_c, True),
                    "Y": Bouncer.mk(YELLOW_c, True),
                    "N": Bouncer.mk(NEUTRAL_c, True),
                    "V": Ventilation.mk('horiz'),
                    "v": Ventilation.mk('vert'),
                    "S": Spawner,
                    ".": lambda _: None
                }[char]
                tool = constructor((x, y))
                self.cells[x, y] = tool

                if tool:
                    tool.state = self.state
                    if isinstance(tool, Ventilation):
                        self.ventilos.append(tool)

        self.npc_to_schedule = level.PNJs
        self.npc_scheduled = []

        super().__init__((0, 0))

        self.tip = None
        if level.tip != None:
            self.tip = image(level.tip)
            self.add_script(self.remove_tip())

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, value):
        self._state = value
        for tool in self:
            tool.state = value

    def __iter__(self) -> Iterator[Tool]:
        return (v for v in self.cells.values() if v)

    def __getitem__(self, item: tuple[int, int]):
        return self.cells[item]

    def around(self, grid_pos, radius=2) -> Iterator[Tool]:
        x, y = grid_pos
        for dx in range(-radius, radius + 1):
            for dy in range(-radius, radius + 1):
                if (tool := self.cells.get((x + dx, y + dy))):
                    yield tool

    @classmethod
    def level(cls, nb: int):
        return cls(LEVELS[nb - 1])

    def logic(self):
        super().logic()

        for tool in self:
            if tool:
                tool.logic()

        # Schedule NPCs
        i = 0
        npc_left_to_schedule = []
        while len(self.npc_scheduled) < NB_PNJs and i < len(self.npc_to_schedule) and (len(self.npc_scheduled) == 0 or self.last_pnj_entry + TIME_BETWEEN_NPCs*self.state.FPS < self.state.timer):
            # check if compatible
            test_pnj = self.npc_to_schedule[i]
            compatible = True
            for pnj in self.npc_scheduled:
                compatible = compatible and (Vector2(pnj.grid_pos) - test_pnj.grid_pos).length() > 3

            if compatible:
                # on schedule le pnj
                self.npc_scheduled.append(self.state.add(test_pnj.instanciate(self.level.draw_border)))
                self.last_pnj_entry = self.state.timer
            else:
                npc_left_to_schedule.append(test_pnj)
            i += 1

        self.npc_to_schedule = npc_left_to_schedule+self.npc_to_schedule[i:]

    def draw(self, gfx: "GFX"):
        super().draw(gfx)

        # gfx.grid(grid_to_rect((0, 0)), (GRID_WIDTH, GRID_HEIGHT), GRID_CELL_WIDTH, WHITE + (40,))

        for tool in self:
            if tool:
                tool.draw(gfx)

        # for x in range(GRID_WIDTH):
        #     for y in range(GRID_HEIGHT):
        #         left, top = grid_to_rect((x, y))
        #         gfx.box(Rect(left+1, top+1, GRID_CELL_WIDTH-2, GRID_CELL_WIDTH-2), Color(0, 0, 0, 128))
        #
        # for pos in LEFT_WALL + RIGHT_WALL:
        #     left, top = grid_to_rect(pos)
        #     gfx.box(Rect(left+1, top+1, GRID_CELL_WIDTH-2, GRID_CELL_WIDTH-2), Color(0, 0, 0, 255))

        if self.tip != None:
            if self.level.tip_pos == TIP_BOTTOM:
                gfx.blit(self.tip, bottomleft=(20, H-50))
            else:
                gfx.blit(self.tip, topleft=(20, 50))



    def remove_tip(self):
        yield from range(60 * self.state.FPS)
        self.tip = None