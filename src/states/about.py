from src.engine import *


class AboutMenu(State):
    BG_COLOR = None

    def draw(self, gfx: "GFX"):
        super().draw(gfx)

        gfx.blit(image("credits"), topleft=(0, 0))