from src.engine import *


class EndMenu(State):
    BG_COLOR = None

    def draw(self, gfx: "GFX"):
        super().draw(gfx)

        gfx.blit(image("endScreen"), topleft=(0, 0))