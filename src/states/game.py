from src import states
from src.engine.state_transitions import ExpandingCircleTransition, FadeTransition, SquarePatternTransition, \
    SquareSpawnTransition
from src.objects.player import Player
from src.objects.npc import Npc
from src.objects.map import LEVELS, Map
from src.objects.timeline import Timeline
from src.engine import *


class GameState(State):
    BG_COLOR = None

    def __init__(self, level=1):
        super().__init__()

        self.level = level
        self.timeline = self.add(Timeline())
        self.map = self.add(Map.level(level))
        self.player = self.add(Player((W / 2, H / 2)))

    def create_inputs(self) -> Inputs:
        inputs = super().create_inputs()
        inputs["skip"] = Button(pygame.K_p).on_press(lambda _: self.next_level())
        return inputs

    def draw(self, gfx: "GFX"):
        super().draw(gfx)

    def next_level(self):
        if self.level == len(LEVELS):
            self.replace_state(
                ExpandingCircleTransition(self, states.EndMenu(), self.player.center)
            )
        else:
            self.replace_state(
                # SquarePatternTransition.circle(self, GameState(self.level + 1), 50, 2, 50)
                ExpandingCircleTransition(self, GameState(self.level + 1), self.player.center)
            )

    def logic(self):
        super().logic()
        if not self.map.npc_to_schedule and not self.map.npc_scheduled:
            self.next_level()

    def handle_events(self, events):
        super().handle_events(events)