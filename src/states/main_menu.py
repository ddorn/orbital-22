from math import cos, sin

import pygame
from pygame import Vector2

from src import states
from src.engine import *
from src.engine.state_transitions import ExpandingCircleTransition


class MenuState(State):
    BG_COLOR = None
    def __init__(self):
        super().__init__()

    def handle_events(self, events):
        super().handle_events(events)

        for event in events:
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    self.push_state(
                        ExpandingCircleTransition(
                            self, states.GameState(),
                            (553, 319)
                        ))

    def draw(self, gfx: "GFX"):
        mouse = Vector2(pygame.mouse.get_pos())
        gfx.blit(image("title"), topleft=(0, 0))
        super().draw(gfx)

        p = Vector2(1000, 170)
        p.y += sin(self.timer / 24) * 10 - sin(self.timer / 12) * 2
        img = image("aboutButton")
        r = img.get_rect(center = p)

        if r.collidepoint(mouse):
            pixel = img.get_at(vec2int(mouse - r.topleft))
            if pixel != (0, 0, 0, 0):
                img = image("aboutButtonLight")

                if pygame.mouse.get_pressed()[0]:
                    self.push_state(
                        ExpandingCircleTransition(
                            self, states.AboutMenu(),
                            mouse
                        ))

        gfx.blit(img, topleft=r.topleft)

        p = Vector2(960, 600)
        p.y += sin(self.timer / 23 + 2) * 24 - sin(self.timer / 12) * 3
        img = image("playButton")
        r = img.get_rect(center = p)

        if r.collidepoint(mouse):
            pixel = img.get_at(vec2int(mouse - r.topleft))
            if pixel != (0, 0, 0, 0):
                img = image("playButtonLight")

                if pygame.mouse.get_pressed()[0]:
                    self.push_state(
                        ExpandingCircleTransition(
                            self, states.GameState(),
                            mouse
                        ))

        gfx.blit(img, topleft=r.topleft)

